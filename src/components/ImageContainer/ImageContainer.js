import React from "react";
import cx from "classnames";
import "./ImageContainer.scss";

export const ImageComponent = props => {
  const { url, customClassName, customImageClassName } = props;

  return (
    <div className={cx("image-component-container", customClassName)}>
      <img
        className={cx("image-component__img img-fluid", customImageClassName, {
          loadingState: !url
        })}
        src={url}
        key={url}
        alt={url}
      />
    </div>
  );
};
