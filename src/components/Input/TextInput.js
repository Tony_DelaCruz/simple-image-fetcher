import React from "react";
import cx from "classnames";
import "./TextInput.scss";

export const TextInput = props => {
  const { value, onChange, placeHolder, customClassName } = props;
  return (
    <input
      value={value}
      placeholder={placeHolder}
      onChange={onChange}
      className={cx("form-control textfield-input", customClassName)}
    />
  );
};
