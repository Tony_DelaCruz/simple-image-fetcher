import React from "react";

export const ButtonComponent = props => {
  const { buttonText, customClassName, onSubmit, disabled } = props;
  return (
    <button
      disabled={disabled}
      className={`btn ${customClassName}`}
      onClick={e => {
        e.preventDefault();
        onSubmit();
      }}
    >
      {disabled ? "Loading" : buttonText}
    </button>
  );
};
