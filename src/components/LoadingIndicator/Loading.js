import React from "react";
import "./Loading.scss";

export const LoadingIndicator = props => {
  const { loadingText, customClassName } = props;
  return (
    <div
      className={`loading-indicator__container d-flex align-items-center justify-content-center ${customClassName}`}
    >
      <div>{loadingText}</div>
    </div>
  );
};
