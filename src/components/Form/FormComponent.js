import React from "react";
import "./FormComponent.scss";

export const FormComponent = props => {
  const { customClassName } = props;
  return (
    <form className={`form-component-container form-inline ${customClassName}`}>
      {props.children}
    </form>
  );
};
