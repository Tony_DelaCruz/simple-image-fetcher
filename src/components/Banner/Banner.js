import React from "react";
import _get from "lodash/get";
import { TextInput } from "../Input/TextInput";
import { FormComponent } from "../Form/FormComponent";
import { ButtonComponent } from "../Button/ButtonComponent";
// import {
//   FaReact,
//   FaGithub,
//   FaCameraRetro,
//   FaBitbucket,
//   FaBootstrap
// } from "react-icons/fa";
import "./Banner.scss";

// const githubLink = process.env.REACT_APP_GITHUB_LINK;
// const bitbucketRepoLink = process.env.REACT_APP_BITBUCKET_REPO_LINK;
// const reactDocsLink = process.env.REACT_APP_REACT_DOCS_LINK;
// const unspashLink = process.env.REACT_APP_UNSPLASH_LINK;
// const bootstrapLink = process.env.REACT_APP_BOOTSTRAP_LINK;

export const Banner = props => {
  const {
    searchQuery,
    setSearchQuery,
    fetchImagesOnSearch,
    disabled,
    scrollOnClick
  } = props;

  const onChange = e => {
    const searchParam = _get(e, "target.value", "");
    setSearchQuery(searchParam);
  };

  const onClickLink = url => {
    window.open(url, "_blank");
  };

  const front = "< Img_";
  const rear = " />";
  return (
    <div className="banner_container">
      <div className="jumbotron mb-0">
        <nav class="navbar p-4 shadow">
          <a class="navbar-brand" href="#">
            <span>{front}</span>
            <span className="splash">SPLASH</span>
            <span>{rear}</span>
          </a>
          <FormComponent customClassName="form-container__banner form-inline">
            <TextInput
              icon={<span class="glyphicon glyphicon-search"></span>}
              value={searchQuery}
              placeHolder="Search for images..."
              onChange={onChange}
              customClassName="banner__text-input"
            />
            <ButtonComponent
              disabled={disabled}
              onSubmit={fetchImagesOnSearch}
              buttonText="Submit"
              customClassName="sidebar__submit-action btn btn-outline-primary"
            />
          </FormComponent>
        </nav>
        <div className="jumbotron-content">
          <h1>High quality images on the regular.</h1>
          <div className="content-description">
            This simple application lets you search and view images from the{" "}
            <span className="font-weight-bold">
              <a>Unsplash</a>
            </span>{" "}
            webiste that houses a wide range of images provided by a community
            of creators.
          </div>
        </div>
        <div className="scroll-btn" onClick={() => scrollOnClick()}>
          <div className="btn-text">View Images </div>
          <i class="fa fa-chevron-down"></i>
        </div>
      </div>
    </div>
  );
};
