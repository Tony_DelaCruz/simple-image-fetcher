import React from "react";
import { LoadingIndicator } from "../LoadingIndicator/Loading";

export const ComponentLoader = props => {
  const { loading } = props;
  return (
    <div>
      {!loading ? (
        props.children
      ) : (
        <LoadingIndicator loadingText="Fetching Images" />
      )}
    </div>
  );
};
