import axios from "axios";

export const getImages = (url, config) => {
  return axios.get(url, config);
};
