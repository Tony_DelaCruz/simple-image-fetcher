import React from "react";
import _isEmpty from "lodash/isEmpty";
import _get from "lodash/get";
import SimpleReactLightbox from "simple-react-lightbox";
import { SRLWrapper } from "simple-react-lightbox";
import { useHistory } from "react-router-dom";

import { getImages } from "../../utils/axiosHelper";

import { Banner } from "../../components/Banner/Banner";
import { ImageComponent } from "../../components/ImageContainer/ImageContainer";
import { ComponentLoader } from "../../components/ComponentLoader/ComponentLoader";

import "./dashboard.scss";

export const DashBoardView = () => {
  const [images, setImages] = React.useState([]);
  const [searchQuery, setSearchQuery] = React.useState("");
  const [loading, setLoading] = React.useState(false);
  const [error, setError] = React.useState(false);
  const history = useHistory();

  const ref = React.createRef();

  const handleScroll = current => {
    current.scrollIntoView({
      behavior: "smooth",
      block: "start",
      inline: "nearest"
    });
  };

  const handleScrollOnClick = () => {
    const { current } = ref;
    if (!_isEmpty(current)) {
      handleScroll(current);
    }
  };

  const fetchImagesOnMount = () => {
    setLoading(true);
    return getImages("https://api.unsplash.com/photos?", {
      params: {
        /**
         * TODO: access key should not be in frontend env when app is deployed.
         */
        client_id: process.env.REACT_APP_ACCESS_KEY,
        per_page: 20
      }
    }).then(data => {
      setLoading(false);
      setImages(_get(data, "data", []));
    });
  };

  const searchImagesRedirect = () => history.push(`/results/${searchQuery}`);

  React.useEffect(() => {
    fetchImagesOnMount();

    return window.localStorage.removeItem("search_term");
  }, []);

  const searchTerm = window.localStorage.getItem("search_term");
  const SRLWrapperOptions = {
    showCaption: false
  };

  return (
    <SimpleReactLightbox>
      <div className="text-center">
        <Banner
          searchQuery={searchQuery}
          setSearchQuery={setSearchQuery}
          fetchImagesOnSearch={searchImagesRedirect}
          disabled={loading}
          scrollOnClick={handleScrollOnClick}
        />
        <div>
          {!error && (
            <ComponentLoader loading={loading}>
              <div className="dashboard" ref={ref}>
                {searchTerm && (
                  <h3 className="dashboard__search-term text-left mt-3 mb-0">
                    Results for:{" "}
                    <span className="search-term__keyword">{searchTerm}</span>
                  </h3>
                )}
                {!_isEmpty(images) && (
                  <SRLWrapper options={SRLWrapperOptions}>
                    <div className="dashboard__image-container card-columns">
                      {images.map(item => {
                        const regularUrl = _get(item, "urls.regular");
                        return <ImageComponent url={regularUrl} />;
                      })}
                    </div>
                  </SRLWrapper>
                )}
              </div>
            </ComponentLoader>
          )
          // :
          // (
          //   <div className="dashboard__error-message d-flex align-items-center justify-content-center">
          //     <h1>
          //       No results found for{" "}
          //       <span className="dashboard__search-term">{`"${searchTerm}"`}</span>
          //     </h1>
          //   </div>
          // )
          }
        </div>
      </div>
    </SimpleReactLightbox>
  );
};
