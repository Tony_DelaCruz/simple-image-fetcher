import React from "react";
import _get from "lodash/get";
import _isEmpty from "lodash/isEmpty";
import { Link } from "react-router-dom";

import SimpleReactLightbox from "simple-react-lightbox";
import { SRLWrapper } from "simple-react-lightbox";

import { getImages } from "../../utils/axiosHelper";

import { ImageComponent } from "../../components/ImageContainer/ImageContainer";

import "./searchResults.scss";

export default ({ match }) => {
  const [images, setImages] = React.useState([]);
  const [searchQuery, setSearchQuery] = React.useState("");
  const [loading, setLoading] = React.useState(false);
  const [error, setError] = React.useState(false);

  const searchTerm = _get(match, "params.search_term", "");

  const fetchImagesOnSearch = async searchTerm => {
    setLoading(true);
    setError(false);
    setSearchQuery("");

    const imagesResponse = await getImages(
      `https://api.unsplash.com/search/photos?`,
      {
        params: {
          query: searchTerm,
          client_id: process.env.REACT_APP_ACCESS_KEY,
          per_page: 25
        }
      }
    );

    const responseData = _get(imagesResponse, "data.results", []);
    if (_isEmpty(responseData)) {
      setLoading(false);
      setError(true);
    } else {
      setLoading(false);
      setImages(responseData);
    }
  };

  React.useEffect(() => {
    fetchImagesOnSearch(searchTerm);
  }, []);

  const SRLWrapperOptions = {
    showCaption: false
  };
  return (
    <div>
      <h1>You have searched for {searchTerm}</h1>
      <button>
        <Link to="/">Back to home page</Link>
      </button>
      <SimpleReactLightbox>
        <SRLWrapper options={SRLWrapperOptions}>
          <div className="searchPage-container card-columns">
            {images &&
              images.map(item => {
                const regularUrl = _get(item, "urls.regular");
                return (
                  <ImageComponent
                    customClassName="searchPage-image-container"
                    customImageClassName="searchPage-image"
                    url={regularUrl}
                  />
                );
              })}
          </div>
        </SRLWrapper>
      </SimpleReactLightbox>
    </div>
  );
};
