import React from "react";
import { DashBoardView } from "./containers/dashboard/dashboard";
import SearchResultPage from "./containers/SearchResultContainer/SearchResultPage";
import { BrowserRouter, Switch, Route } from "react-router-dom";

import "./App.scss";

const pageNotFound = () => <div>Page not found</div>;
function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Switch>
          <Route path="/" exact component={DashBoardView} />
          <Route
            path="/results/:search_term"
            exact
            component={SearchResultPage}
          />
          <Route component={pageNotFound} />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
